# Zabbix to Cachet

This goal of this tool is to synchronize trigger status and metric from Zabbix
to Cachet. Many implementation ideas were drawn from 
[qk4l/zabbix-cachet](https://github.com/qk4l/zabbix-cachet), but this project was created to further extend of this idea and to utilize more of the 

## Configuration

TODO

## Installation
This application is written completely in JavaScript using libraries available
from NPM. There are a couple ways to go about this.

### Via Docker
This method is pretty 

Probably something like:
```bash
docker-compose build
docker-compose -d up
```

### Via nodejs
Ensure you have a supported version of NodeJS through its 
[official repositories](https://nodejs.org/en/download/package-manager/).

Probably something like:
```bash
git clone https://gitlab.com/nanderson94/zabbix-cachet-js.git
cd zabbix-cachet-js
npm install
npm start
```

## Contributing
Pull requests and issues are always welcome, and are the preferred way to
start discussions and propose changes related to this project.

## Testing

TODO

## License
Copyright [2016] [Nicholas Anderson]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
