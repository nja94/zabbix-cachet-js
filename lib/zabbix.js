/*
 *  Copyright 2016 Nicholas Anderson
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const http = require('http');
const w = require('winston');

// Configure logger
w.level = 'LOG_LEVEL' in process.env ? process.env.LOG_LEVEL : 'debug';

class ZabbixAPIError extends Error {

}

 /**
  *  Class for Zabbix RPC interaction
  */
class Zabbix {
  /**
   * constructor - Initializes the API client
   *
   * @param  {String} apiEndpoint Full path to Zabbix's JSON RPC endpoint
   * @param  {String} username    Username to login to Zabbix with
   * @param  {String} password    Password to login to Zabbix with
   * @param  {Object} opts        Configuration options
   */
  constructor(apiEndpoint) {
    let vm = this;
    w.log('debug', 'initalizing Zabbix api to endpoint: %s', apiEndpoint);
    vm._opts = {
      hostname: apiEndpoint,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json-rpc'
      }
    };
    vm._host = apiEndpoint;
    vm.authToken = null;
  }


  /**
   * _req - Internal request handler. Request doesn't do ECMA Promises :(
   *
   * @param  {Object} body  The request body. Object should be JSONifyable
   * @return {Promise}      Resolves with server response, Rejects errors
   */
  _req(body) {
    let vm = this;
    return new Promise((resolve, reject) => {
      let req = http.request(vm._opts);
      req.end(JSON.stringify(body));

      req.on('connect', (res, socket, head) => {
        w.log('debug', 'Sending request to Zabbix endpoint', {
          body: body
        });
      });

      let data = "";
      req.on('data', chunk => {
        data += chunk.toString('utf8');
      });

      req.on('end', () => {
        resolve(data);
      });

      req.on('error', e => {
        w.log('error', 'Zabbix request failed. Rejecting to parent', {
          error: e
        });
        reject(e);
      });
    });
  }

  api(method, args) {
    let vm = this;
    return new Promise((resolve, reject) => {
      var req = {
        jsonrpc: '2.0',
        method: method,
        params: params,
        auth: vm.authToken,
        

      }
    });
  }
}
